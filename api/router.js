const { createUser, getUsers, getUserByUserId, updateUser, deleteUser } = require('./controller');
const router = require('express').Router();

router.post('/', createUser);
router.get('/', getUsers);
router.get('/:key', getUserByUserId);
router.patch('/', updateUser);
router.put('/', updateUser);
router.delete('/:key', deleteUser);

module.exports = router;