const pool = require('../config/database');

module.exports = {
    create: (data, callback) => {
        pool.query("insert into queries (queryKey, queryValue) values (?, ?)", [
            data.queryKey,
            data.queryValue,
        ], (error, results, fields) => {
            if(error) {
                return callback(error)
            }
            return callback(null, results)
        })
        // pool.query(`insert into users (firstName, lastName, email, password, role ) values (?, ?, ?, ?, ?)`, [
        //     data.firstName,
        //     data.lastName,
        //     data.email,
        //     data.password,
        //     data.role,
        // ], (error, results, fields) => {
        //     if(error) {
        //         return callback(error)
        //     }
        //     return callback(null, results)
        // })
    },
    getUsers: callback => {
        pool.query(`select queryKey, queryValue from queries`, [], (error, results, fields) => {
            if(error) {
                return callback(error);
            }
            return callback(null, results);
        })
        // pool.query(`select id, firstName, lastName, email, role from users`, [], (error, results, fields) => {
        //     if(error) {
        //         return callback(error);
        //     }
        //     return callback(null, results);
        // })
    },
    getUserById: (key, callback) => {
        pool.query(`select queryKey, queryValue from queries where queryKey = ?`, [key], (error, results, fields) => {
            if(error) {
                return callback(error);
            }
            return callback(null, results[0]);
        })
    },
    updateUser: (data, callback) => {
        pool.query(`update queries set queryValue=? where queryKey= ?`, [
            data.queryValue,
            data.queryKey,
        ], (error, results, fields) => {
            if(error) {
                return callback(error)
            }
            return callback(null, results)
        })
    },
    deleteUser: (key, callback) => {
        pool.query(`delete from queries where queryKey=?`, [key], (error, results, fields) => {
            if(error) {
                return callback(error)
            }
            return callback(null, results[0])
        })
    }
}