const { create, getUsers, getUserById, updateUser, deleteUser } = require('./model');
// const { genSaltSync, hashSync } = require('bcrypt');
const pool = require('../config/database');


module.exports = {
    createUser : (req, res) => {
        const params = req.query;
        let key = Object.keys(params)[0];
        let value = Object.values(params)[0];
        
        if(!key || !value) {
            return res.status(400).json({
                success: 0,
                message: 'Please Fill your parameter'
            });
        }
        
        // need to check key already exist or not

        let data = {
            queryKey: key,
            queryValue: value,
        }
        create(data, (err, results) => {
            if(err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'Database connection error'
                });
            }
            return res.status(200).json({
                success: 1,
                data: "Data created successfully"
            });
        });
        // const body = req.body;
        // const salt = genSaltSync(10);
        // body.password = hashSync(body.password, salt);
        // create(body, (err, results) => {
        //     if(err) {
        //         console.log(err);
        //         return res.status(500).json({
        //             success: 0,
        //             message: 'Database connection error'
        //         });
        //     }
        //     return res.status(200).json({
        //         success: 1,
        //         data: "User created successfully"
        //     });
        // });
    },
    getUserByUserId: (req, res) => {
        const key = req.params.key;
        getUserById(key, (err, results) => {
            if(err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'Database connection error'
                });
            }
            if(!results) {
                return res.status(200).json({
                    success: 0,
                    message: 'Record not found'
                });
            }
            let newResults = { [results.queryKey] : results.queryValue};
            return res.status(200).json({
                success: 1,
                data: newResults
            });
        })
    },
    getUsers: (req, res) => {
        getUsers((err, results) => {
            if(err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'Database connection error',
                    err: err.toString()
                });
            }
            const jsonResults = results.map(result => {
                return { [result.queryKey] : result.queryValue }
            })
            return res.status(200).json({
                success: 1,
                data: jsonResults
            });
        })

        // getUsers((err, results) => {
        //     if(err) {
        //         console.log(err);
        //         return res.status(500).json({
        //             success: 0,
        //             message: 'Database connection error'
        //         });
        //     }
        //     return res.status(200).json({
        //         success: 1,
        //         data: results
        //     });
        // })
    },
    updateUser: (req, res) => {
        const params = req.query;
        let key = Object.keys(params)[0];
        let value = Object.values(params)[0];
        
        if(!key || !value) {
            return res.status(400).json({
                success: 0,
                message: 'Please Fill your parameter'
            });
        }
        let data = {
            queryKey: key,
            queryValue: value,
        }
        updateUser(data, (err, results) => {
            if(err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'Database connection error'
                });
            }
            if(!results) {
                return res.status(400).json({
                    success: 0,
                    message: 'Failed to update data'
                });
            }
            return res.status(200).json({
                success: 1,
                data: "Data updated successfully"
            });
        })
        // const body = req.body;
        // const salt = genSaltSync(10);
        // body.password = hashSync(body.password, salt);
        // updateUser(body, (err, results) => {
        //     if(err) {
        //         console.log(err);
        //         return res.status(500).json({
        //             success: 0,
        //             message: 'Database connection error'
        //         });
        //     }
        //     if(!results) {
        //         return res.status(400).json({
        //             success: 0,
        //             message: 'Failed to update user'
        //         });
        //     }
        //     return res.status(200).json({
        //         success: 1,
        //         data: "User updated successfully"
        //     });
        // })
    },
    deleteUser: (req, res) => {
        const key = req.params.key;
        deleteUser(key, (err, results) => {
            if(err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'Database connection error'
                });
            }
            return res.status(200).json({
                success: 1,
                data: "Data deleted successfully"
            });
        })
        // const data = req.body;
        // deleteUser(data, (err, results) => {
        //     if(err) {
        //         console.log(err);
        //         return res.status(500).json({
        //             success: 0,
        //             message: 'Database connection error'
        //         });
        //     }
        //     if(!results) {
        //         return res.status(500).json({
        //             success: 0,
        //             message: 'Record not found'
        //         });
        //     }
        //     return res.status(200).json({
        //         success: 1,
        //         data: "User deleted successfully"
        //     });
        // })
    }
}