const express = require('express');
require('dotenv').config();
const app = express();
const userRouter = require('./api/router');

app.use(express.json());
app.use('/api/queries', userRouter);

app.listen(process.env.PORT, () => {
    console.log('Server up and running on port : ', process.env.PORT);
});